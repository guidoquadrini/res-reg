<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
            new Genemu\Bundle\FormBundle\GenemuFormBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new salud\assets\MenuBundle\AssetsMenuBundle(),
            new salud\acceso\ModelBundle\AccesoModelBundle(),
            new salud\acceso\SecurityBundle\AccesoSecurityBundle(),
            new salud\acceso\UsuariosBundle\AccesoUsuariosBundle(),
            new salud\acceso\ProyectosBundle\AccesoProyectosBundle(),
            new salud\ops\LoadBundle\OpsLoadBundle(),
            new salud\ops\ModelBundle\OpsModelBundle(),
            new salud\ops\AdminBundle\OpsAdminBundle(),
            new salud\ops\MainBundle\OpsMainBundle(),
            new salud\ops\MenuBundle\OpsMenuBundle(),
            new salud\sicap\ModelBundle\SicapModelBundle(),
            new salud\hmi2\ModelBundle\Hmi2ModelBundle(),
            new salud\profesionales\ModelBundle\ProfesionalesModelBundle(),
            new salud\mod_sims\ModelBundle\ModSimsModelBundle(),
            new salud\types\CommonBundle\TypesCommonBundle(),
            new salud\types\UbicacionBundle\TypesUbicacionBundle(),
            new salud\ops\AssetsBundle\OpsAssetsBundle(),
            new salud\ops\AccesoBundle\OpsAccesoBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test','dapp'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
